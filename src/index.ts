import { sleep } from "./utils"

export class CDPCommand {
	constructor(
		public readonly method: string,
		public readonly args: any) {}
}

export class ScrapableEvent {
	constructor(
		public readonly op: string,
		public readonly args: any,
		public readonly delay: number) {}
}

export type TransportFn = (command: CDPCommand) => Promise<void>

const createEngine = (dispatch: TransportFn) => ({
	execute: async (evt: ScrapableEvent) => {
		await sleep(evt.delay)
		switch(evt.op) {
			case "goto":
				await dispatch({
					method: "Page.navigate",
					args: {
						url: evt.args.url
					}
				})
		}
	}
})

export default createEngine
