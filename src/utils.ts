declare var setTimeout: any

export const sleep = (timeout: number) =>
	new Promise(resolve => setTimeout(resolve, timeout))
