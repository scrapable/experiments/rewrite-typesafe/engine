import createEngine, { CDPCommand } from "../src"

const noopTransport = jest.fn()

test("simple goto", async () => {
	const engine = createEngine(noopTransport)
	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com"
		},
		delay: 100
	})
	expect(noopTransport).toBeCalledWith(new CDPCommand(
		"Page.navigate",
		{ url: "https://duck.com" }))
})
